from contextlib import nullcontext
import cv2 as cv
import numpy as np
import threading 
import concurrent.futures

# from src.camera import Camera 
from camera import Camera
c0 = Camera(2)
c1 = Camera(3,"left C1")
c2 = Camera(1,"right C2")
c3 = Camera(2,"backright C3")
c4 = Camera(0,"backleft C4")

camExecutors = concurrent.futures.ThreadPoolExecutor(max_workers=4)
stitchers = concurrent.futures.ThreadPoolExecutor(max_workers=1)
# thing = cv.Stitcher.create()
images = []


while True:
    cameras = [ 
        camExecutors.submit(c1.cameraFeed),
        camExecutors.submit(c2.cameraFeed),
        camExecutors.submit(c3.cameraFeed),
        camExecutors.submit(c4.cameraFeed)
    ]

    # if (c3.getFrame() is not None): 
    #     stitch = [
    #         stitchers.submit(c0.stitch({"ret":c3.getRet(), "frame":c3.getFrame()}, {"ret":c4.getRet(), "frame":c4.getFrame()}, thing))
    #     ]
