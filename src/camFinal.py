import cv2 as cv
import numpy as np
import time
  

class Camera:
    def __init__(self, frame_name: str = "Frame", cam_index: int = 0):
        self.frame_name     = str(frame_name+' '+cam_index)
        self.cam_index      = cam_index

    # Getters
    def getFrameName(self):
        return self.frame_name

    #Setters
    def setFrameName(self, frame_name):
        self.frame_name = frame_name



    #methods