import cv2 as cv
import numpy as np
import time
import multiprocessing.queues

class Camera:

    def __init__(self, camIndex = 0, frameName = 'Frame', frame = None):
        self.camIndex = camIndex
        self.frameName = frameName
        self.frame = frame
        self.images = []
        self.ret = None
        self.camQueue = []
    
    def getCamIndex(self):
        return self.camIndex
    def getFrameName(self):
        return self.frameName
    def getFrame(self):
        return self.frame
    def getRet(self):
        return self.ret
    def getImages(self):
        return self.images
    def getCamQueue(self):
        return self.camQueue
    
    def setCamIndex(self, camIndex):
        self.camIndex = camIndex
    def setFrameName(self, frameName):
        self.frameName = frameName
    def setFrame(self, frame):
        self.frame = frame
    def setRet(self, ret):
        self.ret = ret
    def setImages(self, index, frame):
        self.images[index] = frame
    











    def cameraFeed(self):
        cap = cv.VideoCapture(self.getCamIndex(), cv.CAP_DSHOW)
        time.sleep(1)
        while True:
            ret, frame  = cap.read()
            self.setRet(ret)

            if not ret:
                print("Can't recieve frame..."+str(self.getCamIndex()))
                
                break

            gray  = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            self.setFrame(gray)

        # Need to remove below lines since this is only needed to capture frames
            cv.imshow( self.getFrameName(), gray )

            if cv.waitKey(1) == ord('q'):
                break

        cap.release()
        cv.destroyAllWindows()

    
    # def stitch(self, cam1Data, cam2Data, stitcher):
    #     frames = [[cam1Data["frame"],cam2Data["frame"]]]
    #     status, result = stitcher.stitch(frames)
    #     print(status)
        # Able to get frames from selected cameras now I just need to stitch frams


        # print(cam1Data["frame"])
        # print(cam1Data["ret"])
        # if ( cam1Data["ret"] and cam2Data["ret"]):
        #     cv.imshow("cam1", cam1Data["frame"])
        #     cv.waitKey(1)
        #     cv.imshow("cam2", cam2Data["frame"])
        #     cv.waitKey(1)
        # status, result = stitcher.stitch([cam1Data["frame"], cam2Data["frame"]])
        # print(status)
        # print(status==1)
        # print(status)
        # if status == cv.STITCHER_OK:
        #     print("OKAY")

        # if (cam1Data["frame"] is not None and cam1Data["ret"]):
        #     cv.imshow("cam1", cam1Data["frame"])
        #     cv.waitKey(1)
        
        # if (cam2Data["frame"] is not None and cam2Data["ret"]):
        #     cv.imshow("cam2", cam2Data["frame"])
        #     cv.waitKey(1)
 