import json
import logging
from src.logger import Logger
from src.data_holders.profile import Profile



class Constants:
    def __init__(self, user: str = "default"):
        self.logger = Logger(__name__)
        self.user = user
        self.logger_file_location = "src/assets/profiles.json"
        self.extras = {'file_name': __name__, 'user': self.user}
        self.profiles = json.load(open(self.logger_file_location))
        self.profile_settings = self._read_profile()
        self.hud_settings = self._read_hud()
        self._write_profiles()
        
                            
    def _read_profile(self):
        profile = self.profiles['users'][self.user]['characteristics']
        self.logger.info(f'Reading Profile settings length = {len(profile)}', self.extras)
        return profile

    def _read_hud(self, user: str = "default"):
        profile = self.profiles['users'][self.user]['hud']
        self.logger.info(f'Reading Hud settings length = {len(profile)}', self.extras)
        return profile

    def _write_profiles(self):
        print(self.profiles)
        self.profiles['users'][self.user]['characteristics']['hair'] = 'blue'
        print(self.profiles)

        with open(self.logger_file_location, "w") as outfile:
            json.dump(self.profiles, outfile, indent=4)
