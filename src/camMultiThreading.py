import cv2
import threading
import numpy as np

def capture_and_stitch(camera_id, output_lock):
  cap = cv2.VideoCapture(camera_id)
  while True:
    ret, frame = cap.read()
    if not ret:
      break
    width = 320
    height = 480
    # Process and resize the frame (replace with your stitching logic)
    processed_frame = cv2.resize(frame, (width, height))

    # Acquire lock before adding to the stitched frame
    with output_lock:
      stitched_frame = cv2.hconcat([stitched_frame, processed_frame])

def main():
  # Define camera IDs (replace with your camera indices)
  cameras = [0, 1]  # Assuming cameras at index 0 and 1

  # Define output width based on number of cameras
  
  width = int(640 / len(cameras))
  height = 480

  # Initialize stitched frame and lock
  stitched_frame = np.zeros((height, width * len(cameras), 3), dtype=np.uint8)
  output_lock = threading.Lock()

  # Create threads for each camera
  threads = []
  for camera_id in cameras:
    thread = threading.Thread(target=capture_and_stitch, args=(camera_id, output_lock))
    threads.append(thread)
    thread.start()

  # Display the stitched frame
  while True:
    # Acquire lock before displaying to avoid race conditions
    with output_lock:
      cv2.imshow("Stitched Video", stitched_frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
      break

  # Release resources
  for thread in threads:
    thread.join()
  for camera_id in cameras:
    cap = cv2.VideoCapture(camera_id)  # Recreate capture objects for proper release
    cap.release()
  cv2.destroyAllWindows()

if __name__ == "__main__":
  main()