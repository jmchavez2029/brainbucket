import logging


class Logger:
    def __init__(self,file_name:str = __name__, level: enumerate = logging.INFO, logging_file_name: str = 'app.log'):
        self.file_name = file_name
        self.level = level
        self.logging_file_name = logging_file_name
        self.logger = logging.getLogger(self.file_name)
        self.FORMAT = '%(asctime)s %(file_name)s: %(user)s %(message)s'
        logging.basicConfig(filename=self.logging_file_name, level=self.level, format=self.FORMAT)
    
    def get_file_name(self):
        return self.file_name
    def get_level(self):
        return self.level
    def get_logging_file_name(self):
        return self.get_logging_file_name
        
    def set_file_name(self,file_name):
        self.file_name = file_name
    def set_level(self, level):
        self.level = level
    def sete_logging_file_name(self,get_logging_file_name):
        self.logging_file_name = get_logging_file_name
    

    def info(self, msg, extras):
        self.logger.info(msg, extra=extras)
    
    def warn(self, msg, extras):
        self.logger.warning(msg, extra=extras)
    
    def error(self, msg, extras):
        self.logger.error(msg, extra=extras)
    
    def debug(self, msg, extras):
        self.logger.debug(msg, extra=extras)

    def fatal(self, msg, extras):
        self.logger.fatal(msg, extra=extras)
